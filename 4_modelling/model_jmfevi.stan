//--------------------------------------------------------
// Model jmfevi in Stan - model_jmfevi.stan
// Joint model for longitudinal and time-to-event data
// Evolution of LVEF and survival in Stan
// alvarohv
// 2022
//--------------------------------------------------------

data {
  int<lower=0> N_obs;                    // n. observations
  int<lower=0> N_pat;                    // n. patients
  int<lower=0> N_int;                    // n. time intervals
  int<lower=0> N_far;                    // n. pharmaceuticals
  
  int<lower=0> N_cov_chi;                // n. covariates for chi
  int<lower=0> N_cov_delta;              // n. covariates for delta
  int<lower=0> N_cov_nu;                 // n. covariates for nu
  int<lower=0> N_cov_h;                  // n. covariates for h
  
  vector[N_obs] y;                       // LVEF
  row_vector[N_int] times_bar;           // mean time of each interval
  matrix[N_pat, N_cov_chi] Cov_chi;      // covariate matrix for chi
  matrix[N_pat, N_cov_delta] Cov_delta;  // covariate matrix for delta
  matrix[N_pat, N_cov_nu] Cov_nu;        // covariate matrix for nu
  matrix[N_pat, N_cov_h] Cov_h;          // covariate matrix for h
  
  array[N_far] matrix[N_pat, N_int] Far; // list of pharmaceuticals matrices
  
  vector[N_pat] event;                   // event (death o CT)
  matrix[N_pat, N_int] Events;           // event matrix by intervals
  matrix[N_pat, N_int] Times;            // survival matrix by intervals
  
  array[N_obs] int ind_pat;              // patient index vector
  array[N_obs] int ind_int;              // interval index vector
}

transformed data {
}

parameters {
  // y
  real<lower=0> sigma;                   // LVEF variance
  
  // chi
  real<lower=0, upper=100> alpha_chi;    // initial value of chi
  vector[N_cov_chi] beta_chi;            // effect of covariates on chi
  vector[N_pat] chi_hat;                 // non-center parameterization on chi
  real<lower=0> sigma_chi;               // variance of chi
  
  // delta
  real alpha_delta;                      // initial value of delta
  vector[N_cov_delta] beta_delta;        // effect of covariates on delta
  vector[N_pat] delta_hat;               // non-center parameterization on delta
  real<lower=0> sigma_delta;             // variance of delta
  
  // nu
  real<lower=0> alpha_nu;                // initial value of nu
  vector[N_cov_nu] beta_nu;              // effect of covariates on nu
  real<lower=0> sigma_nu;                // variance of nu
  vector<lower=0>[N_pat] nu;             // saturation velocity nu
  
  // F
  vector[N_far] phi;                     // effect of pharmaceuticals
  
  // survival
  vector<lower=0>[N_int] lambda;         // baseline risk by interval
  vector[N_cov_h] gamma;                 // effect of covariates on survival 
  real gamma_mu;                         // effect of LVEF on survival
}

transformed parameters {
  vector[N_pat] chi;                     // initial value of LVEF
  vector[N_pat] delta;                   // increase of LVEF
  
  matrix[N_pat, N_int] mu;               // mean evolution of LVEF
  vector[N_pat] log_h;                   // logarithm of risk function h
  vector[N_pat] log_S;                   // logarithm of survival function S
  
  // parameters of LVEF evolution
  chi   = alpha_chi + Cov_chi * beta_chi + sigma_chi * chi_hat;
  delta = alpha_delta + Cov_delta * beta_delta + sigma_delta * delta_hat;
  
  // LVEF evolution
  mu = rep_matrix(chi + delta, N_int) -
         rep_matrix(delta, N_int) .* exp(-nu * times_bar);
  for(f in 1:N_far){
    mu += phi[f] * Far[f];
  }
  
  // survival
  log_h = Events * log(lambda) + Cov_h * gamma +
            rows_dot_product(Events, gamma_mu * mu);
  log_S = -exp(Cov_h * gamma) .* ((Times .* exp(gamma_mu * mu)) * lambda);
}

model {
  // priors
  alpha_chi ~ normal(25, 20) T[0, 100];
  alpha_delta ~ normal(30, 30);
  alpha_nu ~ normal(2, 1) T[0, ];
  
  chi_hat ~ normal(0, 1);
  delta_hat ~ normal(0, 1);
  
  sigma ~ cauchy(0, 4);
  sigma_chi ~ cauchy(0, 4);
  sigma_delta ~ cauchy(0, 4);
  sigma_nu ~ cauchy(0, 4);
  
  beta_chi ~ normal(0, 4);
  beta_delta ~ normal(0, 4);
  beta_nu ~ normal(0, 4);
  phi ~ normal(0, 4);
  
  lambda ~ gamma(0.1, 1.5);
  gamma ~ normal(0, 2);
  gamma_mu ~ normal(0, 0.01);
  
  nu ~ normal(alpha_nu + Cov_nu * beta_nu, sigma_nu);
  
  // likelihood
  for(o in 1:N_obs){
    target += normal_lpdf(y[o] | mu[ind_pat[o], ind_int[o]], sigma);
  }
  target += sum(event .* log_h + log_S);
}

generated quantities {
}
